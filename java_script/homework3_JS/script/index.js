

let number1 = +prompt("Enter first number");
while (isNaN(number1)) {
    number1 = +prompt("Enter a valid number");
}
let number2 = +prompt("Enter second number");
while (isNaN(number2)) {
    number2 = +prompt("Enter a valid number");
}
let op = prompt("Enter operation type", "+,-,*,/");
while ((op !="+") && (op != "-") && (op != "*") && (op !="/")) {
    op = prompt("Enter a valid operation ('+, -, *, /')" )
}
let result;

function cal(num1, num2, op) {

    if (op == "+") {
        result = num1 + num2;
    }else if (op == "-") {
        result = num1 - num2;
    }else if (op == "*") {
        result = num1 * num2;
    }else if (op == "/") {
        result = num1 / num2;
    }
        return result;
}
console.log(cal(number1, number2, op));